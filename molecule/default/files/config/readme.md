We use the default encryption scheme (bcrypt)

use `doveadm pw -p <passwd>` to generate the new password.

Password the user `test` is `testpasswd`.

